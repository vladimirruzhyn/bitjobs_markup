$(document).ready(function() {
	$('.popups-links a').magnificPopup({
    type: 'inline'
  });
  
  $("input[type=file]").change(function(){
     var filename = $(this).val().replace(/.*\\/, "");
     $(this).next().text(filename);
  });  
});